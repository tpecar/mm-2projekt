package la;

import java.util.Arrays;

/**
 * Staticne operacije za delo z vektorji ter matrikami v double tabelah.
 * Vredno je pomniti, da se vse transformacije z vektorjem mnozijo iz leve
 * strani, torej v' = M*v
 */
public class MatrikaOp
{
    /** Naredi kopijo vektorja.
     *  Uporabi znotraj bisekcije ter Newtonove metode, kjer lahko pokvarimo
     *  vektor.
     * @param A vektor, ki jo zelimo kopirati
     * @return identicni vektor z drugo referenco
     */
    public static double[] kopiraj(double[] A)
    {
        return Arrays.copyOf(A, A.length);
    }
    
    /** Prepise drugi vektor z vrednostmi prvega.
     *  Pri bisekciji moramo ohranjati reference, da imamo cim dlje iste tabele.
     * @param A izvorni vektor
     * @param B ponorni vektor, v katero pisemo vrednosti A
     * @return referenca na vektor, katerega smo prepisali
     */
    public static double[] prepisi(double[] A, double[] B) {
        System.arraycopy(A, 0, B, 0, A.length);
        return B;
    }
    
    /** Matricno mnozenje.
     * @param A matrika oblike nxk
     * @param B matrike oblike kxm
     * @return matricni zmnozek */
    public static double[][] mnoziMatricno(double[][] A, double[][] B)
    {
        if(A[0].length!=B.length)
            throw new IllegalArgumentException(String.format("Neujemanje matrik [n=%d k=%d] [k=%d m=%d]",A.length,A[0].length,B.length,B[0].length));
        double C[][] = new double[A.length][B[0].length];
        
        /* gremo po komponentah izhodne matrike */
        for(int i=0; i<A.length; i++)
            for(int j=0; j<B[0].length; j++)
                /* gremo po vrstici/stolpcu vhodnih */
                for(int k=0; k<A[0].length; k++)
                    C[i][j] += A[i][k]*B[k][j];
        return C;
    }
    
    /** Skalarni produkt.
     * 
     * @param A stolpcni vektor
     * @param B stolpcni vektor
     * @return skalarni produkt
     */
    public static double mnoziSkalarno(double[] A, double[] B)
    {
        if(A.length!=B.length)
            throw new IllegalArgumentException(String.format("Neujemanje vektorjev [n=%d] [k=%d]",A.length,B.length));
        double rezultat = 0;
        for(int i=0; i<A.length; i++)
            rezultat+=A[i]*B[i];
        return rezultat;
    }
    
    /** Sesteje istolezne komponente.
     * @param A vektor
     * @param B vektor (enako velik kot A)
     * @return sestevek po komponentah */
    public static double[] sestej(double[] A, double[] B)
    {
        if(A.length != B.length)
            throw new IllegalArgumentException(String.format("Neujemanje vektorjev [n=%d] [n=%d]",A.length,B.length));
        double C[] = new double[A.length];
        
        return hitroSestej(A, B, C);
    }
    /** Hitro sesteje vektorja, pri cemer ne kreira nove ponornega vektorja.
     * @param A vektor
     * @param B vektor iste dolzine kot A (brez preverjanja)
     * @param ponor vektor kamor shranimo sestevke
     * @return ponorni vektor
     */
    public static double[] hitroSestej(double[] A, double[] B, double[] ponor)
    {
        for(int i=0; i<A.length; i++)
                ponor[i] = A[i] + B[i];
        return ponor;
    }
    
    /** Mnozi s skalarjem.
     * @param A vektor, ki ga mnozimo s skalarjem
     * @param skalar skalar
     * @return nov vektor, pomnozen s skalarjem
     */
    public static double[] skaliraj(double[] A, double skalar)
    {
        double C[] = new double[A.length];
        return hitroSkaliraj(A, skalar, C);
    }
    /** Hitro mnozenje s skalarjem, ki ne kreira nove ponornega vektorja.
     * 
     * @param A vektor, katerega podatke skaliramo
     * @param skalar s katerim mnozimo
     * @param ponor vektor, kamor skalirano vrednost shranimo
     * @return referenca na ponorni vektor
     */
    public static double[] hitroSkaliraj(double[] A, double skalar, double[] ponor)
    {
        for(int i=0; i<A.length; i++)
                ponor[i] = A[i]*skalar;
        return ponor;
    }
    
    /** Kombinirana operacija sestevanja ter skaliranja, ki uporablja isto
     *  izvorno ter ponorno tabelo, zato da se lahko izognemo kreiranju novih
     *  vektorjev.
     *  Namen tega je pospesiti premike zarkov ter konvergencne metode, ker
     *  ker trenutno so te zaradi alokacije novega pomnilnika pocasne.
     *  Prav tako se zelimo izogniti vec posamicnim zankam.
     * 
     * @param A vektor, katerega pristevamo
     * @param B vektor, katerega mnozimo s skalarjem
     * @param skalar s katerim mnozimo vektor B
     * @return referenca na spremenjen vhodni vektor A
     */
    public static double[] hitroPristejSkalirano(double[] A, double[] B, double skalar)
    {
        for(int i=0; i<A.length; i++)
                A[i] = A[i]+B[i]*skalar;
        return A;
    }
    
    /** Dolžina vektorja.
     * @param A 3D stolpcni vektor
     * @return dolzina
     */
    public static double dolzina(double[] A)
    {
        return Math.sqrt(mnoziSkalarno(A, A));
    }
    
    /** Normalizira vektor.
     * @param A 3D stolpcni vektor
     * @return dolzina
     */
    public static double[] normiraj(double[] A)
    {
        return skaliraj(A, 1/dolzina(A));
    }
    
    /**
     * Tvori 3D Eulerjevo rotacijsko matriko.
     * Glej https://en.wikipedia.org/wiki/Euler_angles
     * Uporabljamo razvoj za X1Y2Z3 rotacijsko matriko, pri cemer sem popravil
     * koeficiente, ker morajo koti 1,2,3 dejansko biti 2,3,1, prav tako je
     * za psi (kot 1) potrebno zamenjati predznak, da bo obrat negativno
     * predznacen.
     * 
     * Za to bi sicer rad videl dejansko izpeljavo namesto necesa, kar sicer
     * dela, vendar pa je bilo ugotovljeno s probavanjem.
     * 
     * Nekoliko intuitivnejsa razlaga tega bi bila, da transformacija po
     * eulerjevih kotih obrne celoten sistem tako, da obrne doloceno ravnino
     * za dan kot.
     * 
     * Pa ne pozabi, da je pozitiven kot v nasprotni smeri urinega kazalca.
     * 
     * @param fi kot, za katerega obracamo xy ravnino
     * @param theta kot, za katerega obracamo zx ravnino
     * @param psi kot, za katerega obracamo yz ravnino
     * @return 3D rotacijsko matriko za podane kote
     */
    public static double[][] eulerRot(double fi, double theta, double psi)
    {
        /* 1 - fi, 2 - theta, 3 - psi*/
        double c1 = Math.cos(fi),
               c2 = Math.cos(theta),
               c3 = Math.cos(psi);
        
        double s1 = Math.sin(fi),
               s2 = Math.sin(theta),
               s3 = Math.sin(psi);
        
        return new double[][]
        {
            {          c1*c2,            -c1*s2,        -s1},
            { c3*s2+c2*s3*s1,    c3*c2-s3*s1*s2,      c1*s3},
            {-s3*s2+c3*c2*s1,   -c2*s3-c3*s1*s2,      c3*c1}
        };
    }
    /** tvori 3D stolpcni vektor iz 1D tabele
     * @param tab tabela
     * @return stolpcni vektor, ki ga je mogoce uporabljati znotraj drugih
     *         matricnih operacij
     */
    public static double[][] stolpcni(double[] tab)
    {
        double vektor[][] = new double[tab.length][1];
        for(int i=0; i<tab.length; i++)
            vektor[i][0]=tab[i];
        return vektor;
    }
    /** tvori 1D tabelo iz 3D stolpcnega vektorja
     * @param vektor stolpcni vektor
     * @return tabela
     */
    public static double[] tabela(double[][] vektor)
    {
        double tabela[] = new double[vektor.length];
        for(int i=0; i<vektor.length; i++)
            tabela[i]=vektor[i][0];
        return tabela;
    }
    
    /* prikaz vektorja v obliki niza */
    public static String niz(double[] A, int stDecimalk)
    {
        StringBuilder niz = new StringBuilder();
        niz.append("[");
        for(int i=0; i<A.length; i++)
        {
            niz.append(String.format("%-"+(stDecimalk+3)+"."+stDecimalk+"f"+(i<A.length-1?" ":""),A[i]));
        }
        niz.append("]");
        return niz.toString();
    }
    
    /* prikaze vektor */
    public static void prikazi(double[] A, int stDecimalk)
    {
        System.out.println(niz(A,stDecimalk));
    }
    
    /* za namen hitrega testiranja */
    /*
    // trenutno neuporabno
    public static void main(String[] args)
    {
        double A[] = {{1,2},{3,4}};
        prikazi(A,0);
        prikazi(mnoziMatricno(A, A),0);
        
        prikazi(skaliraj(A,-1),0);
        
        prikazi(sestej(A,A),0);
        
        double B[][] = {{1},{1},{1},{1}};
        prikazi(B,0);
        prikazi(mnoziMatricno(trans(new double[][]{{1},{2},{3},{0}}), B),0);
        
        prikazi(eulerRot(Math.PI/4,0,0),4);
        // obrat za 90 stopinj v razlicne smeri (pi/2)
        prikazi(mnoziMatricno(eulerRot(Math.PI/4,0,0), new double[][]{{1},{0},{0},{0}}), 4);
    }
    */
}
