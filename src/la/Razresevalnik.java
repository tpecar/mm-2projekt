package la;

import raytracing.Objekt;
import raytracing.Zarek;
import raytracing.objekti.Krogla;

/**
 *  Metode za numericno razresevanje enacb.
 *  Kar je treba pri dani implementaciji metod paziti, je, da te pri zacetni
 *  vrednosti pricakujejo pozitivno vrednost funkcije - torej da ji za zacetno
 *  tocko podamo zadnjo pozicijo zarka, se predno je to slo skozi telo.
 * 
 *  Metode po uspesni konvergenci spremenijo pozicijo zarka na boljsi priblizek
 *  - ali je metoda konvergirala, lahko izvemo iz tega ali je stevilo
 *  porabljenih iteracij pozitivno
 *  (ce smo dosegli nedefinirano obmocje funkcije ali pa presegli stevilo
 *  iteracij, metode vrnejo -1)
 */
public class Razresevalnik {
    /**
     * Poisce presecisce premice (podane s tocko ter smernim vektorjem - zarek)
     * ter objektom preko Newtonove metode za iskanje nicel.
     * 
     * Funkcija objekta (ter njen odvod) mora ze biti nastavljena tako, da je
     * f(x,y,z)=0 na povrsini objekta.
     * 
     * POZOR: dana metoda je misljena za dolocitev natancnejse resitve potem
     *        ko nam je ze dober priblizek, pridobljen preko bisekcije, znan.
     * 
     * @param zarek pozicija ter smer zarka
     * @param objekt za katerega iscemo secisce z zarkom
     * @param korak premika (enako koraku pri zarku)
     * @param toleranca odmik od dejanske nicle funkcije, ki ga se toleriramo
     * @param stKorakov stevilo dovoljenih korakov za konvergenco - ce to
     *                  preseze, obravnavamo kot da ni presecisca
     * @return stevilo iteracij, potrebnih za konvergenco - ce metoda ni
     *         konvergirala, vrne -1
     */
    public static int secisceNewton(Zarek zarek, Objekt objekt, double korak, double toleranca, int stKorakov) {
        /* delamo na kopiji pozicije samega zarka ter mu jo priredimo, ce
         * konvergenca uspesna (pri neuspesni konvergenci smo lahko namrec
         * pridelali nesmiselne koordinate)
         */
        double[] tocka = MatrikaOp.kopiraj(zarek.pozicija);
        
        int opravljenihKorakov=0;
        double vrednost;
        do {
            /* ce pridemo v obmocje, kjer funkcija ali njen odvod ni
               izracunljiv */
            if(opravljenihKorakov==stKorakov || !objekt.definiran(tocka))
                return -1;
            /* dolocimo novo tocko */
            tocka = MatrikaOp.hitroPristejSkalirano(tocka, zarek.smer, korak);
                    //MatrikaOp.sestej(tocka, MatrikaOp.skaliraj(zarek.smer, korak));
            
            /* dolocimo nov korak - glej Lesarjevo izpeljavo v
               Vizualizacija medicinskih volumetricnih podatkov v realnem casu,
               str. 46 */
            korak = korak - objekt.funkcija(tocka)/
                            MatrikaOp.mnoziSkalarno(objekt.odvod(tocka), zarek.smer);
            
            vrednost = objekt.funkcija(tocka);
            //MatrikaOp.prikazi(tocka, 6);
            opravljenihKorakov++;
        } while(Math.abs(vrednost)>toleranca);
        
        /* pridelali veljavno resitev */
        zarek.pozicija = tocka;
        return opravljenihKorakov;
    }
    /**
     * Poisce zacetni priblizek presecisca premice z objektom preko bisekcije.
     * 
     * Edino kar moramo paziti pri dani implementaciji, je da ne postavimo
     * zacetne vrednosti za preseciscem, ker drugace bo metoda nasla zadnjo
     * ploskev namesto prve oz. ne bo sploh konvergirala
     * 
     * @param zarek pozicija ter smer zarka
     * @param objekt za katerega iscemo secisce z zarkom
     * @param korak premika (enako koraku pri zarku)
     * @param toleranca odmik od dejanske nicle funkcije, ki ga se toleriramo
     * @param stKorakov stevilo dovoljenih korakov za konvergenco - ce to
     *                  preseze, obravnavamo kot da ni presecisca
     * @return stevilo iteracij, potrebnih za konvergenco - ce metoda ni
     *         konvergirala, vrne -1
     */
    public static int seciscaBisekcija(Zarek zarek, Objekt objekt, double korak, double toleranca, int stKorakov)
    {
        /* delamo na kopiji pozicije, jo priredimo nazaj zarku, ce uspesno
           konvergiramo (pri bisekciji bi skoraj morali, razen v primeru ko
           naletimo na obmocje, kjer funkcija ni definirana) - z razliko od
           newtonove metode, bisekcija zagotovo vrne natancnejsi priblizek tudi
           ce ji ni uspelo doseci tolerance, vendar pa to lahko pomeni, da
           Newtonova metoda ne bo iterirala */
        double[] tockaSp = MatrikaOp.kopiraj(zarek.pozicija);
        double[] tockaZg = MatrikaOp.sestej(zarek.pozicija, MatrikaOp.skaliraj(zarek.smer, korak));
        double[] tockaMid = new double[zarek.pozicija.length];
        int opravljenihKorakov = 0;
        double vrednost;
        do {
            //System.out.format("%s - %s\n",MatrikaOp.niz(tockaSp,7),MatrikaOp.niz(tockaZg,7));
            
            if(opravljenihKorakov==stKorakov || !objekt.definiran(tockaSp) ||
               !objekt.definiran(tockaZg))
                return -1;
            /* dolocimo vmesno tocko */
            if(Math.abs(objekt.funkcija(tockaSp))<toleranca)
            {
                zarek.pozicija = tockaSp;
                return opravljenihKorakov;
            }
            if(Math.abs(objekt.funkcija(tockaZg))<toleranca)
            {
                zarek.pozicija = tockaZg;
                return opravljenihKorakov;
            }
            
            tockaMid = MatrikaOp.hitroSkaliraj(MatrikaOp.hitroSestej(tockaSp, tockaZg, tockaMid), 0.5, tockaMid);
            /* dolocimo vrednost za sredinsko tocko */
            vrednost = objekt.funkcija(tockaMid);
            /* glede na predznak vzamemo spodnji ali zgornji interval */
            if(vrednost<0)
                /* namesto spremembe reference le prepisemo vrednost */
                MatrikaOp.prepisi(tockaMid, tockaZg);
            else
                MatrikaOp.prepisi(tockaMid, tockaSp);
            opravljenihKorakov++;
            //System.out.printf("     BIS V  %-20.18f\n", vrednost);
        } while(Math.abs(vrednost)>toleranca);
        
        /* pridelali veljavno resitev */
        zarek.pozicija = tockaMid;
        return opravljenihKorakov;
    }
    
}
