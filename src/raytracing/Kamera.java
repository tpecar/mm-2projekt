package raytracing;

/**
 * Okvirna implementacija kamere. - ta mora omogocati
 * - izracun smernega vektorja ter pozicije zarka na zacetni render (D) ploskvi
 * - izracun koncnih x,y,z koordinat zarka z upostevanjem pozicije ter
 *   rotacije kamere
 * Ce si mi predstavljamo, da je obmocje med izhodiscem ter ploskvijo zacetka
 * izrisa neuporabno (vmesni prostor fiktivno mora obstajati le pri
 * perspektivni projekciji, zato da dolocimo smer zarka), ga lahko postavimo kar
 * na xy ravnino in iz tam direktno tvorimo zarke.
 * S tem poenostavijo zadeve na sledec nacin:
 *  - vsi zarki so usmerjeni v +z, zato ploskve, ki ostajajo v -z (za xy
 *    ravnino) nikoli ne srecamo
 *  - zarek lahko premikemo direktno od z=0 naprej, ni nam potrebno upostevati
 *    vmesnega mrtvega prostora
 */
public abstract class Kamera
{
    /** Velikost zaslona/risalne povrsine. Mi namrec za vsak piksel zaslona
     *  generiramo en zarek. */
    int zaslonX,
        zaslonY;
    /** Velikost ploskve zacetnega vidnega prostora. Na to ploskev bodo
     *  razprsene zacetne pozicije zarkov - to torej definira koliksen objekt
     *  lahko se zajamemo v celoti, ce je ta prakticno prislonjen na kamero.
     *
     *  Da si (predvidoma) poenostavimo pozicioniranje objektov, se ta zacetna
     *  ploskev prestavi tako, da z os gre skozi njeno sredisce.
     */
    double vidnoPoljeSirina,
           vidnoPoljeVisina;
    /* Vidno polje postavimo tako, da z os prebada njegovo sredisce
     * Ker je zaslon usmerjen tako, da ima 0,0 v zgornjem levem kotu,
     * zacnemo z zgornjo levo koordinato vidnega polja, nato pa padamo
     * proti negativnemu.
     */
    double vidnoX0 = vidnoPoljeSirina/2,
           vidnoY0 = vidnoPoljeVisina/2;
    /* medsebojni odmik zarkov na zacetni ploskvi */
    double razmakZarkovX = vidnoPoljeSirina/(double)zaslonX,
           razmakZarkovY = vidnoPoljeVisina/(double)zaslonY;
    
    /** Pozicija kamere v prostoru. Ta je podana s koordinatami prostora, v
     *  katerem se nahajajo objekti, ki jih opazujemo.
     */
    double pozicija[];
    /** Smer kamere v prostoru. Ta je podana kot roll (kot po yz ravnini),
     *  pitch (xy), yaw (zx) kot.
     *  Za natancnejso definicijo teh glej Computer Graphics 267, ali na
     *  wikipediji pod Euler Angles - pod Rotation Matrix v tabeli na poziciji
     *  2,2 (Tait-Bryan angles) je enačba, ki jo predvidoma rabimo za
     *  transformacijo.
     *  Je pa pri tem nevarnost ti. Gimbal lock fenomena, s katerim lahko
     *  izgubimo eno od vrtenja - bomo videli kako bo to vplivalo na sistem.
     */
    double smer[];
    
    /**
     * @param zaslonX stevilo pikslov v sirino za zaslon (na katerega risemo
     *                koncno sliko)
     * @param zaslonY stevilo pikslov v visino
     * @param vidnoPoljeSirina sirina vidnega polja v sirino
     * @param vidnoPoljeVisina sirina vidnega polja v visino
     * @param pozicija pozicija kamere v prostoru
     * @param smer roll, pitch, yaw
     */
    public Kamera(int zaslonX, int zaslonY,
                  double vidnoPoljeSirina, double vidnoPoljeVisina,
                  double[] pozicija,
                  double[] smer) {
        this.zaslonX = zaslonX;
        this.zaslonY = zaslonY;
        this.vidnoPoljeSirina = vidnoPoljeSirina;
        this.vidnoPoljeVisina = vidnoPoljeVisina;
        this.pozicija = pozicija;
        this.smer = smer;
    }
    
    /**
     * Vrnemo zacetni zarek za podan xy zaslona.
     * Dobljene koordinate so koordinate dejanskega zaslona oz. risalne povrsine
     * (torej celostevilske vrednosti) - te prvo pretvorimo v koordinate
     * nasega vidnega prostora, nato pa glede na te dolocimo se usmerjenost
     * vektorja.
     * @param x koordinata x piksla
     * @param y koordinata y piksla
     * @return zacetna smer in pozicija zarka
     */
    public abstract Zarek zacetniZarek(int x, int y);
    
    /**
     * Klonira kamero.
     * Potrebujemo za paralelni raytrace.
     * 
     * @return novo instanco kamere z istimi parametri (razen pomoznih atributov).
     */
    public abstract Kamera kloniraj();
}
