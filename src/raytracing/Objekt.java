package raytracing;

/** Predstavlja 3D objekt, opisan s funkcijo. Pri tem mora veljati pogoj, da
    je na obmocju, kjer je funkcija objekta definirana, ta tudi odvedljiva.
    */
public abstract class Objekt {
    
    /* atributi, skupni vsem objektom */
    /** Pozicija objekta v prostoru, oziroma odmik funkije iz koordinatnega
        izhodisca. */
    public double[] pozicija;
    
    /* skupni konstruktor */
    
    public double[] barva;
    public Objekt(double[] pozicija, double[] barva) {
        this.pozicija = pozicija;
        this.barva = barva;
    }
    
    /** Vrne ali je funkcija definirana za izracun/odvajanje v tej tocki.
        V primeru ce ni, obravnavamo kot, da tam ploskve ni in posledicno lahko
        gre zarek skozi.
     * @param tocka tocka (x,y,z), za katero preverjamo ce je funkcija
     *                 veljavna
     * @return ali je funkcija veljavna */
    public abstract boolean definiran(double[] tocka);
    
    /** Vrne vrednost funkcije f(x,y,z) v dani poziciji tocke.
     * @param tocka vrednosti prostih spremenljivk x,y,z
     * @return vrednost funkcije */
    public abstract double funkcija(double[] tocka);
    
    /** Vrne parcialni odvod funkcije f'(x,y,z) v dani poziciji tocke.
     * @param tocka tocka (x,y,z)
     * @return parcialni odvod funkcije po x, y, z */
    public abstract double[] odvod(double[] tocka);
}
