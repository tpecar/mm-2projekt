/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raytracing.objekti;

import raytracing.Objekt;

/**
 *
 * @author Peter
 */
public class Ravnina  extends Objekt
{
    private double[] normala;
    

    public Ravnina(double[] pozicija, double[] normala, double[] barva)
    {
        super(pozicija, barva);
        this.normala = normala;
    }
    
    @Override
    public boolean definiran(double[] tocka)
    {
        return true;
    }

    @Override
    public double funkcija(double[] tocka)
    {
        return (tocka[0] - pozicija[0])*normala[0] + (tocka[1] - pozicija[1])*normala[1] + (tocka[2] - pozicija[2])*normala[2];
    }

    @Override
    public double[] odvod(double[] tocka)
    {
       return normala;
    }
    
}
