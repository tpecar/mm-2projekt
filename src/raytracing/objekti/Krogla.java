package raytracing.objekti;

import raytracing.Objekt;

public class Krogla extends Objekt {
    
    double r;
    
    /* konstruktor */
    public Krogla(double[] pozicija, double[] barva, double r) {
        super(pozicija, barva);
        this.r = r;
    }
    
    /* funkcija krogle je povsod definirana */
    @Override
    public boolean definiran(double[] tocka) {
        return true;
    }
    /* funkcija oblike f(x,y,z)=(x-x0)^2+(y-y0)^2+(z-z0)^2-r^2
       idealno je njena vrednost 0 (ce je tocka na sami krogli) */
    @Override
    public double funkcija(double[] tocka) {
        return Math.pow(tocka[0]-pozicija[0],2)+
               Math.pow(tocka[1]-pozicija[1],2)+
               Math.pow(tocka[2]-pozicija[2],2)-
               Math.pow(r,2);
    }
    /* funkcija oblike Jf(x,y,z)=[2(x-x0), 2(y-y0), 2(z-z0)] */
    @Override
    public double[] odvod(double[] tocka) {
        return new double[]{
            2*(tocka[0]-pozicija[0]),
            2*(tocka[1]-pozicija[1]),
            2*(tocka[2]-pozicija[2])
        };
    }
}
