/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raytracing.objekti;

import raytracing.Objekt;

/**
 *
 * @author Peter
 */
public class Torus extends Objekt
{
    private double[] radija;
    
    public Torus(double[] pozicija, double[] radija, double[] barva)
    {
        super(pozicija, barva);
        this.radija = radija;
    }
    
    @Override
    public boolean definiran(double[] tocka)
    {
        return true;
    }

    @Override
    public double funkcija(double[] tocka)
    {
       double sq = Math.sqrt((tocka[0] - pozicija[0])*(tocka[0] - pozicija[0]) + (tocka[1] - pozicija[1])*(tocka[1] - pozicija[1]));
       return Math.pow((radija[0] - sq),2) + Math.pow((tocka[2]- pozicija[2]),2) - radija[1]*radija[1];
    }

    @Override
    public double[] odvod(double[] tocka)
    {
        double sq = Math.sqrt((tocka[0] - pozicija[0])*(tocka[0] - pozicija[0]) + (tocka[1] - pozicija[1])*(tocka[1] - pozicija[1]));
        return new double[]{2*(radija[0] - sq)*(-0.5*2*((tocka[0] - pozicija[0])/sq)), 2*(radija[0] - sq)*(-0.5*2*((tocka[1] - pozicija[1])/sq)), 2*(tocka[2] - pozicija[2])};
    }
    
}
