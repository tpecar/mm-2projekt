package raytracing;

import gui.RisalnaPovrsina;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Prevzame celotno funkcionalnost enonitnega raytracerja, le da izvaja celotno
 * zadevo vzporedno za vec kamer.
 */
public class ParalelniRayTracer extends RayTracer {
    
    /* atributi */
    int stNiti;
    
    /* konstruktor */
    public ParalelniRayTracer(Kamera kamera, Objekt[] objekti, double[] virSvetlobePozicija,
                              double korak, int maxKorakov, int stNiti)
    {
        super(kamera, objekti, virSvetlobePozicija, korak, maxKorakov);
        this.stNiti = stNiti;
    }
    
    /** Paralelni izris.
     */
    @Override
    public void izrisi(RisalnaPovrsina povrsina) {
        if(povrsina.buffer!=null) {
            int sirina = povrsina.buffer.getWidth();
            int visina = povrsina.buffer.getHeight();
            /* dolocimo koliko vrstic bo posamezna nit obdelala */
            int vrsticNaNit = visina/stNiti;
            
            /* ustvarimo vse izvajalnike */
            ArrayList<Delavec> delavci = new ArrayList<>();
            for(int tDelavec=0; tDelavec<stNiti; tDelavec++)
                delavci.add(new Delavec(tDelavec*vrsticNaNit,
                           (tDelavec+1)*vrsticNaNit, sirina, kamera.kloniraj()));
            
            ExecutorService izvajalnik = Executors.newFixedThreadPool(stNiti);
            List <Future<Delavec>> rezultat=null;
            try {
                rezultat = izvajalnik.invokeAll(delavci);
            } catch (InterruptedException e) {
                System.out.format("NAPAKA MAP NITI %s\n",e.toString());
            }
            try {
                if(rezultat!=null) {
                    for(Future<Delavec> tDelavecRezultat : rezultat) {
                        /* prepisemo na dejanski zaslon */
                        Delavec tDelavec = tDelavecRezultat.get();
                        for(int y=tDelavec.zacY; y<tDelavec.konY; y++) {
                            for(int x=0; x<sirina; x++) {
                                povrsina.pixel(x, y,
                                        tDelavec.rezultat[y-tDelavec.zacY][x][0],
                                        tDelavec.rezultat[y-tDelavec.zacY][x][1],
                                        tDelavec.rezultat[y-tDelavec.zacY][x][2]);
                            }
                        }
                    }
                }
            } catch (ExecutionException | InterruptedException e) {
                System.out.format("NAPAKA REDUCE NITI %s\n",e.toString());
            }
        }
    }
    /**
     * Izvajalnik za posamezno nit.
     * Sicer zlorabljam dejstvo, da bom vrnil kar celoten objekt, ki je
     * izvajal servis, da lahko pridobim njegovo interno stanje - to NI
     * lepo programiranje.
     * 
     * To pocnem zato, ker je izvajanje funkcij asinhrono, s tem pa nam lahko
     * katerakoli prileti prva nazaj - zato moramo vedeti katere vrstice je
     * ta objekt obdeloval da jih lahko narisemo na povrsino.
     */
    class Delavec implements Callable<Delavec> {
        public int rezultat[][][];
        /* obdelamo celotne vrstice, povemo od kod do kod */
        public int zacY, konY;
        int sirina;
        /* instanca kamere, ki jo uporabljamo za izrisovanje - vsaka nit ima
           svojo */
        Kamera tKamera;
        
        /* konstruktor */

        public Delavec(int zacY, int konY, int sirina, Kamera tKamera) {
            this.zacY = zacY;
            this.konY = konY;
            this.sirina = sirina;
            /* alociramo tabelo za rezultate */
            rezultat = new int[konY-zacY][sirina][3];
            
            this.tKamera = tKamera;
        }
        @Override
        public Delavec call() {
            for(int y=zacY; y<konY; y++) {
                for(int x=0; x<sirina; x++) {
                    rezultat[y-zacY][x] = vrniPiksel(x, y, tKamera);
                }
            }
            return this;
        }
    }
}
