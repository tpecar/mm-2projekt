/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raytracing;

import la.MatrikaOp;

public class PersKamera extends Kamera
{
    /** Oddaljenost ocisca kamere od vidnega polja (zacetne ploskve gledanega
     *  prostora, iz katere izhajajo zarki in ki se preslika na zaslon) */
    private final double [] d;
    /* vektor odmika od sredisca do roba vidne ploskve po x osi (sirini) */
    private final double [] s;
    /* vektor odmika od sredisca do roba vidne ploskve po y osi (visini) */
    private final double [] v;
    /* oba vektorja odmika sta pozitivno usmerjena */
    
    /* vidni kot v stopinjah */
    private double kot = 25;
    
    /* OPTIMIZACIJA: atributi, ki uporabljajo isto referenco, zato da
       se izognemo pretirani alokaciji.
       POTREBNO JE PAZITI da v vecnitnem sistemu vsaka nit potrebuje svojo
       instanco kamere.
    */
    private final double [] sMod;
    private final double [] vMod;
    private final double [] normSmer; /* normirana smer */
    private final Zarek zarekMod;
    
    public PersKamera(int zaslonX, int zaslonY,
                        double vidnoPoljeSirina, double vidnoPoljeVisina,
                        double[] pozicija,
                        double[] smer,
                        double kot)
    {
        super(  zaslonX, zaslonY,
                vidnoPoljeSirina, vidnoPoljeVisina,
                pozicija,
                smer);
        
        this.kot = kot;
        
        /* zaenkrat je d po privzetem usmerjen na z os */
        d = new double[]{0,0,(vidnoPoljeSirina/2)/Math.tan(kot*Math.PI/180)};
        s = new double[]{vidnoPoljeSirina/2, 0, 0};
        v = new double[]{0, vidnoPoljeVisina/2, 0};
        
        /* inicializacija prostora za pomozne atribute */
        sMod = new double[3];
        vMod = new double[3];
        
        normSmer = new double[3];
        zarekMod = new Zarek(new double[3], new double[3]);
        /*
        System.out.format("PersKamera INIT: d %s | s %s | v %s\n",
                MatrikaOp.niz(d,3),
                MatrikaOp.niz(s,3),
                MatrikaOp.niz(v,3));*/
    }

    public double[] svetloba(int x, int y)
    {
        /* izdelamo zarek za kamero v koordinatnem izhodiscu */
        double[] out = MatrikaOp.sestej(MatrikaOp.hitroSkaliraj(s, 0.5 - ((double)x)/zaslonX, sMod), MatrikaOp.hitroSkaliraj(v, 0.5 - ((double)y)/zaslonY, vMod));
        out = MatrikaOp.sestej(d, out);
        
        /* dolocimo normirano smer */
        out = MatrikaOp.skaliraj(out, 1/MatrikaOp.dolzina(out));
        
        /* premaknemo ocisce kamere (ter s tem vse vektorje - zarke) na dejansko
         * lokacijo ter dolocimo smerni vektor */
        return MatrikaOp.sestej(pozicija, out);
    }
    
    @Override
    public Zarek zacetniZarek(int x, int y)
    {
        /* izdelamo zarek za kamero v koordinatnem izhodiscu */
        MatrikaOp.hitroSestej(MatrikaOp.hitroSkaliraj(s, 0.5 - ((double)x)/zaslonX, sMod), MatrikaOp.hitroSkaliraj(v, 0.5 - ((double)y)/zaslonY, vMod), zarekMod.pozicija);
        MatrikaOp.hitroSestej(d, zarekMod.pozicija, normSmer);
        
        /* dolocimo normirano smer */
        MatrikaOp.hitroSkaliraj(normSmer, 1/MatrikaOp.dolzina(normSmer), normSmer);
        
        /* premaknemo ocisce kamere (ter s tem vse vektorje - zarke) na dejansko
         * lokacijo ter dolocimo smerni vektor */
        return new Zarek(MatrikaOp.hitroSestej(pozicija, zarekMod.pozicija, zarekMod.pozicija), normSmer);
    }
    
    @Override
    public Kamera kloniraj() {
        /* naredimo novo instanco kamere ter prevezemo kar se da
           (in kar se ne spreminja), vse ostalo kopiramo */
        return  new PersKamera(
                super.zaslonX,super.zaslonY,
                super.vidnoPoljeSirina, super.vidnoPoljeVisina,
                /* pozicija ter smer vseh instanc kamer mora biti ista, tudi po
                   premiku, zato uporabljamo isto referenco! */
                super.pozicija,
                super.smer, this.kot);
    }
}
