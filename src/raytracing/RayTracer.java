package raytracing;

import gui.RisalnaPovrsina;
import java.util.Random;
import la.MatrikaOp;
import la.Razresevalnik;

/** Zadolzen za simulacijo zarkov, ter njihovo preslikavo na
    piksle zaslona.
    Dan razred hrani svet kamere ter objektov - mi od kamere zahtevamo, da nam
    za podan piksel vrne zarek z usmeritvijo, tega nato posljemo skozi prostor
    dokler ne zadane prvi objekt (presecisce moramo preveriti za vsakega izmed
    objektov), ali pa preseze maksimalno dolzino vektorja zarka.
    
    V primeru, da zarek seka povrsino objekta, dolocimo utezeno povprecje med
    - svetlobo, ki pride direktno od vira svetlobe (imeli bomo en vir) ter
    - svetlobo, ki pride od popolnega odboja svetlobe od drugega objekta.
    */
public class RayTracer {
    /* atributi sveta */
    public Kamera kamera;
    Objekt[] objekti;
    
    /* atributi vira svetlobe - mi bomo zaenkrat privzeli tockast vir, ki oddaja
       v vse smeri */
    public double[] virSvetlobePozicija;
    
    /* atributi same simulacije */
    /* sam korak premika po prostoru */
    double korak;
    /* maksimalen faktor (dejansko dolzina vektorja oz. oddaljenost od kamere,
     * ker je smerni vektor predvidoma normiran), predno nehamo simulacijo
     * - to bi bilo v bistvu dobro za preveriti kaj je sploh smiselno gledati
     * 
     * -> s stalisca casovne zahtevnosti je namrec bolj smiselno gledati stevilo
     *    korakov, ki jih opravimo,
     *    s stalisca natancnejsega nadzora veliksoti risalnega prostora pa
     *    oddaljenost
     * 
     * Zaenkrat bom sel s korakom.
     */
    int maxKorakov;
    
    /* konstruktor */
    public RayTracer(Kamera kamera, Objekt[] objekti, double[] virSvetlobePozicija, double korak, int maxKorakov) {
        this.kamera = kamera;
        this.objekti = objekti;
        this.virSvetlobePozicija = virSvetlobePozicija;
        this.korak = korak;
        this.maxKorakov = maxKorakov;
    }
    
    /** Izvede simulacijo za podan piksel.
     *  Zaradi samega nacina obravnave problema bi se lahko izvajanje dane
     *  metode paraleliziralo s tem, da vec niti racuna vec kosov slike -
     *  najbolje bi bilo, ce bi vzporedno racunali v interlaved nacinu, da
     *  dobimo celotno sliko ze vmes, nato pa se samo dodajajo manjkajoce
     *  vrstice.
     *  Vrne RGB vrednost piksla v celostevilskih vrednostih med 0 in 255.
     * 
     *  Tu podamo se instanco kamere, ki mora biti unikatna za vsako nit.
     */
    int[] vrniPiksel(int x, int y, Kamera tKamera) {
        double[] rgb = vrniSvetlostZarka(tKamera.zacetniZarek(x, y), 100);
        //System.out.printf("SVETLOST %s",MatrikaOp.niz(rgb, 3));
        return new int[]{(int)(rgb[0]*255), (int)(rgb[1]*255), (int)(rgb[2]*255)};
    }
    /**
     * Izvede simulacijo za zarek - poslje zarek ter gleda kdaj zadane povrsino.
     * Ta z zarkom dobi zacetni T ter s, ter ona simulira presecisce z objektom
     * - ce ne najde presecisca do max lambda, vrne temo
     * - ce najde presecisce, doloci lambert, T ter s popolnega odboja ter
     *   rekurzivno kliče za ta dva parametra
     * @param zarek zacetni zarek, ki ga premikamo po prostoru - pri tem bodi
     *              pozoren, da se tekom simulacije stanje podanemu zarku
     *              spremeni (ce zelis ohraniti njegovo vrednost, ga moras
     *              klonirati)
     * @return vrednosti RGB, normalizirane med 0 in 1
     */
    public double[] vrniSvetlostZarka(Zarek zarek, int globina) {
        
        if (globina == 0)
            return new double[]{0,0,0};
        //Začetna pozicija.
        double[] start = MatrikaOp.kopiraj(zarek.pozicija);
        //if (globina == 0)
        //    return new double[]{0, 0, 0};
       // System.out.format("TRACE START %s - %s\n",MatrikaOp.niz(zarek.pozicija, 3),MatrikaOp.niz(zarek.smer, 3));
        /* tabela prejsnjih vrednosti objektov funkcij - dolocimo zacetne
           vrednosti teh */
        double prejsnjaVrednost[] = new double[objekti.length];
        for(int tObjekt=0; tObjekt<objekti.length; tObjekt++)
            prejsnjaVrednost[tObjekt] = objekti[tObjekt].funkcija(zarek.pozicija);
        
        for(int tKorak=0; tKorak<maxKorakov; tKorak++)
        {
            /* naredimo korak */
            zarek.premakni(korak);
            /* pogledamo, ce se je za katerokoli funkcijo spremenil predznak:
                - ce se je, koncamo s premikanjem ter dolocimo natancno
                  pozicijo ter svetlost
                - ce se ni, shranimo novo vrednost funkcije ter nadaljujemo
                  z drugimi objekti */
            for(int tObjekt=0; tObjekt<objekti.length; tObjekt++) {
                double vrednost = objekti[tObjekt].funkcija(zarek.pozicija);
                /* ce zadeli, poiscemo natancnejsi priblizek - ce metode ne
                   konvergirajo, ignoriramo */
                
                if(vrednost>0 != prejsnjaVrednost[tObjekt]>0) {
                    //System.out.format("INITIAL HIT AT RAY POS %s PVAL %-6.4f TVAL %-6.4f\n", MatrikaOp.niz(zarek.pozicija, 4),prejsnjaVrednost[tObjekt],vrednost);
                    /* premaknemo se korak nazaj, da bomo pred povrsino */
                    zarek.premakni(-korak);
                    
                    int stKorakBis,
                        stKorakNew=-1;
                    /* dolocimo svetlost le ce metodi konvergirata za presecisce */
                    if(
                    /* pri bisekciji uporabimo za zacetek negativen korak, da
                       se premaknemo nazaj na obmocje, znoraj katerega je
                       presecisce */
                       (stKorakBis = Razresevalnik.seciscaBisekcija(zarek, objekti[tObjekt], korak, 1e-4, 200))>=0 &&
                       (stKorakNew = Razresevalnik.secisceNewton(zarek, objekti[tObjekt], 1e-10, 1e-7, 500))>=0) {
                    /*    
                    System.out.format("HIT AT RAY POS %s PVAL %-6.4f TVAL %-6.4f [bis %4d] [new %4d]\n",
                            MatrikaOp.niz(zarek.pozicija, 4),prejsnjaVrednost[tObjekt],vrednost,
                            stKorakBis,stKorakNew);
                        */
                        /* dolocili veljavno presecisce */
                        //System.out.format("   HIT [bis %4d] [new %4d] AT %s\n",stKorakBis,stKorakNew,MatrikaOp.niz(zarek.pozicija, 4));

                        double [] norm = objekti[tObjekt].odvod(zarek.pozicija);
                        double [] light = MatrikaOp.sestej(MatrikaOp.skaliraj(zarek.pozicija, -1), virSvetlobePozicija);
                        double angl = Math.acos(MatrikaOp.mnoziSkalarno(norm, light)/(MatrikaOp.dolzina(norm)*MatrikaOp.dolzina(light)));
                        //System.out.println(String.format("The calculated angle: %f\n", angl));
                        //System.out.format("   HIT KOT %-6.4f rad, [%-6.4f deg]\n",angl,angl*(180.0/Math.PI));
                        if (angl >= 0 && angl <= Math.PI/2) 
                        {
                            double lumi = 1 - Math.abs(angl)/(Math.PI/2);
                            double[] zacBarva = new double[]{objekti[tObjekt].barva[0]*lumi, objekti[tObjekt].barva[1]*lumi, objekti[tObjekt].barva[2]*lumi};
                            
                            double[] vhodniVektor = MatrikaOp.sestej(start ,zarek.pozicija);
                            double[] normiranNormalni = MatrikaOp.normiraj(norm);
                            double[] novaSmer = MatrikaOp.sestej(vhodniVektor, MatrikaOp.skaliraj(normiranNormalni, -2*MatrikaOp.mnoziSkalarno(vhodniVektor, normiranNormalni)));
 
                            zarek.smer = MatrikaOp.normiraj(novaSmer);//MatrikaOp.normiraj(new double[]{rnd.nextDouble(), rnd.nextDouble(), rnd.nextDouble()});
                            zarek.premakni(0.5);
                            double[] dodatnaBarva = vrniSvetlostZarka(zarek, globina - 1);
                            //poljubno skaliraj
                            
      
                            double [] barva = MatrikaOp.sestej(MatrikaOp.skaliraj(zacBarva, 0.7), MatrikaOp.skaliraj(dodatnaBarva, 0.3));
                            //System.out.println(barva[0] + " " + barva[1] + " " + barva[2]);
                            return barva;
                        }
                        /* ce je kot prevelik, se nic svetlobe ne odbije */
                        return new double[]{0.0,0.0,0.0};
                    }
                    /*
                    else
                        System.out.format("   DID NOT CONVERGE [bis %4d] [new %4d]\n", stKorakBis,stKorakNew);*/
                    //System.out.format("   ST IT %3d %3d\n",stKorakBis,stKorakNew);
                }
  
                /* ce se nismo zaceli vracati poskusimo drugi objekt */
                prejsnjaVrednost[tObjekt] = vrednost;
            }
        }
        /* v danem stevilu korakov nismo se nic zadeli, vrnimo temo */
        return new double[]{0,0,0};
    }
    /**
     * Izris slike na podano risalno povrsino.
     * Risalna povrsina mora ze biti vzpostavljena - mi iz nje pridobimo njeno
     * sirino ter visino ter na podlagi tega za vsak njen piksel naredimo
     * izracun svetlosti.
     * @param povrsina "zaslon" na katerega risemo piksle
     */
    public void izrisi(RisalnaPovrsina povrsina) {
        if(povrsina.buffer!=null) {
            int sirina = povrsina.buffer.getWidth();
            int visina = povrsina.buffer.getHeight();
            for(int y=0; y<visina; y++) {
                for(int x=0; x<sirina; x++) {
                    int[] rgb = vrniPiksel(x, y, kamera);
                    povrsina.pixel(x, y, rgb[0], rgb[1], rgb[2]);
                }
            }
        }
    }
}
