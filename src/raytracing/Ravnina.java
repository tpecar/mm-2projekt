/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raytracing;

import java.util.Arrays;

/**
 *
 * @author Peter
 */
public class Ravnina
{
    //Točka ki definira ravnina
    private double[][] tocka;
    private double[][] normala;
    
    public Ravnina(double[][] tocka, double[][] normala)
    {
        this.tocka = tocka;
        this.normala = normala;
    }
    
    public boolean definiran(double[][] pozicija)
    {
        return true;
    }

    public double funkcija(double[][] pozicija)
    {
        return (pozicija[0][0] - tocka[0][0])*normala[0][0] + (pozicija[1][0] - tocka[1][0])*normala[1][0] + (pozicija[2][0] - tocka[2][0])*normala[2][0];
    }

    public double[][] odvod(double[][] pozicija)
    {
        return Arrays.copyOf(normala, normala.length);
    }
    
}
