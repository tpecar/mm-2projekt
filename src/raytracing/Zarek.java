package raytracing;

import la.MatrikaOp;

/**
 * Hrani pozicijo ter normirano smer zarka.
 */
public class Zarek
{
    /** 3D stolpcni vektor z w=1. (4 komponente, definira tocko v 3D prostoru -
     * povezano s transformacijami, glej Computer Graphics, str 234) */
    public double[] pozicija;
    /** 3D stolpcni vektor z w=0. (definira vektor smeri v 3D prostoru)*/
    public double[] smer;
    
    /* konstruktor */
    public Zarek(double[] pozicija, double[] smer) {
        this.pozicija = pozicija;
        this.smer = smer;
    }
    
    /** Premakne zarek v smeri smernega vektorja za podan korak.
     * @param korak skalar, s katerim mnozimo normirani vektor smeri zarka
     */
    public void premakni(double korak)
    {
        pozicija = MatrikaOp.hitroPristejSkalirano(pozicija, smer, korak);
        //MatrikaOp.sestej(pozicija, MatrikaOp.skaliraj(smer, korak));
    }
    
    /**
     * Vrnemo zarek, ki kaze relativno na objekt s podano pozicijo ter smerjo.
     * 
     * Za objekte, ki niso v koordinatnem izhodiscu sistema, vendar ga ti
     * predpostavljajo (kamera v standardni legi ter funkcije objektov), moramo
     * zarek iz dejanskega vidnega polja preslikati relativno glede na objekt
     * tako, da bo ta v izhodiscu brez rotacij, med tem ko sam zarek premaknemo
     * ter rotiramo tako, da je ta se vedno na isti oddaljenosti od objekta.
     * 
     * Pri tem nam ni potrebno upostevati smeri zarka, saj nas ta zanima le
     * ob premiku zarka, tu pa ga obravnavamo kot tocko.
     * @param pozicijaObjekta dejanska pozicija objekta, ki ga premikamo v
     *                        izhodisce
     * @param roll kot psi, obrat yz ravnine (glej eulerRot v MatrikaOp za
     *             podrobnosti)
     * @param yaw kot theta, obrat zx ravnine
     * @param pitch kot fi, obrat xy ravnine
     * @return koordinate tocke, transformirane za objekt v izhodiscu
     */
    /*
    // trenutno neuporabno
    public double[][] prestaviZaObjekt(double [][] pozicijaObjekta,
                                       double roll, double yaw, double pitch) {
        // tvorimo transformacijsko matriko s tem, da naredi premik, nato pa
        //   to rotira.
        return  MatrikaOp.mnoziMatricno(MatrikaOp.eulerRot(-roll, -yaw, -pitch),
                    MatrikaOp.mnoziMatricno(MatrikaOp.trans(MatrikaOp.skaliraj(pozicijaObjekta, -1)), pozicija)
                );
    }
    */
}
