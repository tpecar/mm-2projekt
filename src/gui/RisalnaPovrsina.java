package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class RisalnaPovrsina extends JPanel
{
    /** Slika z risalno povrsino, na katero risemo. */
    public BufferedImage buffer;
    
    /* privzeti konstruktor - zato, da ga lahko GUI urejevalnik Netbeansa
       pozre */
    public RisalnaPovrsina()
    {
        super();
    }
    /* vzpostavi risalno povrsino z necim smiselnim - od zacetka tega namrec
       ne moremo, ce zelimo to imeti dodano kot komponento v GUI editorju + 
       radi bi spreminjali dimenzije ipd. */
    public void vzpostavi(int sirina, int visina)
    {
        /* popravimo dimenzijo povrsine ter prisilimo layout manager, da to
           uposteva*/
        setPreferredSize(new Dimension(sirina, visina));
        
        /* vzpostavimo risalno povrsino */
        GraphicsConfiguration gKonf = GraphicsEnvironment.
                getLocalGraphicsEnvironment().
                getDefaultScreenDevice().
                getDefaultConfiguration();
        buffer = gKonf.createCompatibleImage(sirina, visina);
    }
    /* metoda za pisanje RGB.
       Trenutno to ni najbolj optimalna varianta, bomo videli do kod nas
       to privede - je pa to najenostavnejsi nacin, da prevalis celotno
       skrb glede barvnih modelov na Javo.
    */
    public void pixel(int x, int y, int R, int G, int B)
    {
        buffer.setRGB(x, y, R<<16 | G<<8 | B);
    }
    /* zapolne celotno polje z dano RGB vrednostjo */
    public void fill(int R, int G, int B)
    {
        Graphics2D g = buffer.createGraphics();
        g.setPaint(new Color(R, G, B));
        g.fillRect(0, 0, buffer.getWidth(), buffer.getHeight());
    }
    
    /* metoda za izris */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        /* izrisemo le ce smo vzpostavili buffer */
        if(buffer!=null)
            g.drawImage(buffer, 0, 0, this);
        
    }
}
