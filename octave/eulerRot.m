function v = eulerRot(fi, theta, psi)
% dober nacin testiranja je
%hold on; rot = eulerRot(pi/4,0,0); vecx = rot*(t.*[1;0;0;0]); vecy = rot*(t.*[0;1;0;0]); vecz = rot*(t.*[0;0;1;0]); plot3([vecx(1,:) vecy(1,:) vecz(1,:)],[vecx(2,:) vecy(2,:) vecz(2,:)],[vecx(3,:) vecy(3,:) vecz(3,:)]); plot3(vecx(1,1),vecx(2,1),vecx(3,1),'ro');plot3(vecy(1,1),vecy(2,1),vecy(3,1), 'go')

    c1 = cos(fi);
    c2 = cos(theta);
    c3 = cos(psi);
    
    s1 = sin(fi);
    s2 = sin(theta);
    s3 = sin(psi);

    v=[
                 c1*c2,            -c1*s2,        -s1, 0;
        c3*s2+c2*s3*s1,    c3*c2-s3*s1*s2,      c1*s3, 0;
        -s3*s2+c3*c2*s1,    -c2*s3-c3*s1*s2,      c3*c1, 0;
                     0,                 0,          0, 1;
    ];