function v = eulerRot(fi, theta, psi)
% dober nacin testiranja je
% vec = eulerRot(0,0,pi/2)*(t.*[1;0;0;0]); plot3(vec(1,:),vec(2,:),vec(3,:)); plot3(vec(1,1),vec(2,1),vec(3,1),'ro')
    c2 = cos(fi);
    c3 = cos(theta);
    c1 = cos(psi);
    
    s2 = sin(fi);
    s3 = sin(theta);
    s1 = sin(psi);
    
    % prvotna X1Y2Z3 definicija na wikipediji, vendar pri tem se pozitiven psi
    % obraca v negativno smer
    %v=[
    %             c2*c3,            -c2*s3,         s2, 0;
    %    c1*s3+c3*s1*s2,    c1*c3-s1*s2*s3,     -c2*s1, 0;
    %    s1*s3-c1*c3*s2,    c3*s1+c1*s2*s3,      c1*c2, 0;
    %                 0,                 0,          0, 1;
    %];
    
    v=[
                 c2*c3,            -c2*s3,        -s2, 0;
        c1*s3-c3*s1*s2,    c1*c3+s1*s2*s3,     -c2*s1, 0;
        s1*s3+c1*c3*s2,    c3*s1-c1*s2*s3,      c1*c2, 0;
                     0,                 0,          0, 1;
    ];